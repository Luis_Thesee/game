package gamestore;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class VideGameTest 
{
    @Test
    public void videoGameGetType()
    {
        VideoGame vg = new VideoGame("Video Game","cod",100,"action",3,"raven",100);
        assertEquals( vg.getType(), "Video Game");        // equals( true );
    }

    @Test
    public void videoGameGetTitle()
    {
        VideoGame vg = new VideoGame("Video Game","cod",100,"action",3,"raven",100);
        assertEquals( vg.getTitle(), "cod");        // equals( true );
    }

    @Test
    public void videoGameGetPrice()
    {
        VideoGame vg = new VideoGame("Video Game","cod",100,"action",3,"raven",100);
        assertEquals( vg.getPrice(), 100,0.00001);
    }

    @Test
    public void videoGameGetGenre()
    {
        VideoGame vg = new VideoGame("Video Game","cod",100,"action",3,"raven",100);
        assertEquals( vg.getGenre(), "action");        // equals( true );
    }

    @Test
    public void videoGameMinPlayer()
    {
        VideoGame vg = new VideoGame("Video Game","cod",100,"action",3,"raven",100);
        assertEquals( vg.getMinPlayer(), 3);
    }

    @Test
    public void videoGameGetCompany()
    {
        VideoGame vg = new VideoGame("Video Game","cod",100,"action",3,"raven",100);
        assertEquals( vg.getCompany(), "raven");        // equals( true );
    }

    @Test
    public void videoGameGetMemorySize()
    {
        VideoGame vg = new VideoGame("Video Game","cod",100,"action",3,"raven",100);
        String expected="Type: Video Game, Title: cod, Price: 100.0, Genre: action, MinPlayer: 3, Company: raven, MemorySize: 100.0";
        assertEquals( expected,vg.toString());
        
    }
}
