package gamestore;

import static org.junit.Assert.*;

import org.junit.Test;

public class BoardGameTest {
    
    @Test
    public void boardGameGetType()
    {
        BoardGame bg = new BoardGame("Board Game","Monopoly",30,"family",2,"hasebros",40);
        assertEquals( bg.getType(), "Board Game");
    }

    @Test
    public void boardGameGetTitle()
    {
        BoardGame bg = new BoardGame("Board Game","Monopoly",30,"family",2,"hasebros",40);
        assertEquals( bg.getTitle(), "Monopoly");
    }

    @Test
    public void boardGameGetPrice()
    {
        BoardGame bg = new BoardGame("Board Game","Monopoly",30,"family",2,"hasebros",40);
        assertEquals( bg.getPrice(), 30,0.000001);
    }

    @Test
    public void boardGameGetGenre()
    {
        BoardGame bg = new BoardGame("Board Game","Monopoly",30,"family",2,"hasebros",40);
        assertEquals( bg.getGenre(), "family");
    }


    @Test
    public void boardGameGetMinPlayer()
    {
        BoardGame bg = new BoardGame("Board Game","Monopoly",30,"family",2,"hasebros",40);
        assertEquals( bg.getMinPlayer(), 2);
    }

    @Test
    public void boardGameGetCompany()
    {
        BoardGame bg = new BoardGame("Board Game","Monopoly",30,"family",2,"hasebros",40);
        assertEquals( bg.getCompany(), "hasebros");
    }

    @Test
    public void boardGameGetBoardSize()
    {
        BoardGame bg = new BoardGame("Board Game","Monopoly",30,"family",2,"hasebros",40);
        assertEquals( bg.getBoardSize(), 40,0.001);
    }

    @Test
    public void boardGameToString()
    {
        BoardGame bg = new BoardGame("Board Game","Monopoly",30,"family",2,"hasebros",40);
        System.out.println(bg);
        String expected="Type: Board Game, Title: Monopoly, Price: 30.0, Genre: family, MinPlayer: 2, Company: hasebros, BoardSize: 40.0";
        assertEquals(expected,bg.toString());
    }

   
}
