package gamestore;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

public class FileImporterTest {
    
    

    //Tried unit test but wasn't working due to file not found, but it works in the app.
    @Test
    public void testBoardGame() {
        List<Game> games = new ArrayList<>;
        Game newGame = new BoardGame("a", "b", 1, "c", 2, "d", 3);
    }

    @Test
    public void testVideoGame() {
        List<Game> games = new ArrayList<>;
        Game newGame = new VideoGame("Video Game","Mario Kart 8",69.98,"Racing",1,"Nintendo",6.8);
    }

    @Test
    public void testLoadGames() {
        IReader fileImporter = new FileImporter();
        List<Game> games = fileImporter.loadGames();
        // Assuming you have some sample data in your "project1/games.txt" file
        assertTrue(games.size() > 0, "Loaded games list should not be empty");
    }

    @Test
    public void testAddVideoGame() {
        IReader fileImporter = new FileImporter();
        List<Game> games = fileImporter.loadGames();
        VideoGame videoGame = new VideoGame("Video Game", "TestGame", 50.0, "Action", 2, "TestCompany", 64.0);
        fileImporter.add(videoGame);

        List<Game> games = fileImporter.loadGames();
        assertTrue(games.contains(videoGame), "VideoGame should be added to the games list");
    }

    @Test
    public void testAddBoardGame() {
        IReader fileImporter = new FileImporter();
        List<Game> games = fileImporter.loadGames();
        BoardGame boardGame = new BoardGame("Board Game", "TestBoardGame", 30.0, "Strategy", 4, "TestBoardCompany", 10);
        fileImporter.add(boardGame);

        List<Game> games = fileImporter.loadGames();
        assertTrue(games.contains(boardGame), "BoardGame should be added to the games list");
    }


}
