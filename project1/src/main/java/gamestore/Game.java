package gamestore;

public abstract class Game {
    private String type;
    private String title;
    private double price;
    private String genre;
    private int minPlayer;
    private String company;

    public Game(String type,String title, double price, String genre, int minPlayer, String company)
    {
        this.type=type;
        this.title=title;
        this.price=price;
        this.genre=genre;
        this.minPlayer=minPlayer;
        this.company=company;
    }

    public String getType() {
        return this.type;
    }
    public String getTitle() {
        return this.title;
    }

    public double getPrice() {
        return this.price ;
    }

    public String getGenre() {
        return this.genre;
    }

    public int getMinPlayer() {
        return this.minPlayer;
    }

    public String getCompany() {
        return this.company;
    }

    @Override
    public String toString()
    {
        return "Type: "+this.type+", Title: "+this.title+", Price: "+this.price+", Genre: "+this.genre+", MinPlayer: "+this.minPlayer+", Company: "+this.company;
    }
   


}
