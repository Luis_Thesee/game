package gamestore;

public class UserDisplay implements IGameDisplayer{
    @Override
    public void Display(Game game) {
        System.out.println("Type: " + game.getType() + " Title: " + game.getTitle() + " Price: " + game.getPrice());

        if ("Video Game".equalsIgnoreCase(game.getType()) && game instanceof VideoGame) {
            System.out.println("Memory Size: " + ((VideoGame) game).getMemorySize());
        } else if ("Board Game".equalsIgnoreCase(game.getType()) && game instanceof BoardGame) {
            System.out.println("Board Size: " + ((BoardGame) game).getBoardSize());
        } else {
            System.out.println("Unknown game type or invalid object!");
        }
    }
    

}
