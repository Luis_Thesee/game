package gamestore;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.nio.file.StandardOpenOption;

public class FileImporter implements IReader{

    private List<Game> game;

    public FileImporter(){
        this.game = new ArrayList<Game>(); 
    }

    public List<Game> loadGames(){
        try{
            Path path = Paths.get("project1/games.txt");
            List<String> rows = Files.readAllLines(path);
 
            
            for (String row : rows) {
                String[] columns = row.split("\t");
                if("Board Game".equals(columns[0])){
                    BoardGame b = new BoardGame(columns[0], columns [1], Double.parseDouble(columns[2]), columns[3], Integer.parseInt(columns[4]), columns[5], Double.parseDouble(columns[6]));
                    this.game.add(b);
                }
                else{
                    VideoGame v = new VideoGame(columns[0], columns [1], Double.parseDouble(columns[2]), columns[3], Integer.parseInt(columns[4]), columns[5], Double.parseDouble(columns[6]));
                    this.game.add(v);
                }
            
            }
            
        }
        catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    // System.out.println(game);
    return this.game;
    }

    public void add(Game game){

        List<String> newGame = new ArrayList<String>();
        try{
            if(game.getType() == "Video Game"){
                String g = "\n" +  game.getType() + "\t" + game.getTitle() + "\t" + game.getPrice() + "\t" + game.getGenre() + "\t" + game.getMinPlayer() + "\t" + game.getCompany() + "\t" + ((VideoGame)game).getMemorySize();
                newGame.add(g);
                this.game.add(game);
            }

        else{
            String g = "\n" + game.getType() + "\t" + game.getTitle() + "\t" + game.getPrice() + "\t" + game.getGenre() + "\t" + game.getMinPlayer() + "\t" + game.getCompany() + "\t" + ((BoardGame)game).getBoardSize();
            newGame.add(g);
            this.game.add(game);
        }

        Files.write(Paths.get("project1/games.txt"), newGame, StandardOpenOption.APPEND);
        System.out.println("good");
        }catch(IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    public void updateGamePrice(String title, double newPrice){
        //Use the method in sql only
    }
}
