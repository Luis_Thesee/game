package gamestore;

public class VideoGame extends Game{

    private double memorySize;

    public VideoGame(String type,String title, double price, String genre, int minPlayer, String company, double memorySize) {
        super(type,title,price,genre,minPlayer,company);
        this.memorySize=memorySize; 
    }

    public void setMemorySize(double memorySize) {
        this.memorySize=memorySize;
    }

    public double getMemorySize() {
        return memorySize;
    }

    @Override
    public String getType() {
        return super.getType();
    }

    @Override
    public String toString()
    {
        return super.toString()+", MemorySize: "+this.memorySize;
    }
    

}
