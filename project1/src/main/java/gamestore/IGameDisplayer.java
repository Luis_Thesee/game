package gamestore;


public interface IGameDisplayer {
    public void Display(Game game);
}
