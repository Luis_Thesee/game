package gamestore;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App 
{
    public static void main( String[] args ){
        Scanner scan = new Scanner(System.in);;
        mainPage(scan);

    }

    public static void mainPage(Scanner scan)
    {

        boolean value=true;
        while(value)
        {
            System.out.println("0: Exit");
            System.out.println("1: Admin");
            System.out.println("2: Customer");
            System.out.println("Enter Character");
            int num=Integer.parseInt(scan.nextLine());

            switch(num){

                case 0:
                    value=false;
                    break;
                case 1:
                    Admin(scan);
                    break;
                case 2:
                    Customer(scan);
                    break;
                default:
                    System.out.println("invalid input");
            }
        }
    }


    public static void Admin(Scanner scan)
    {
        IReader ir=new FileImporter();
        List<Game> gamesList=ir.loadGames();
        IGameDisplayer ad = new AdminDisplay();
        boolean value=true;
        while(value)
        {
            System.out.println("0: Return");
            System.out.println("1: Display Products");
            System.out.println("2: Update Product Price");
            System.out.println("3: Add Product");
            System.out.println("Enter Character");
            int num=Integer.parseInt(scan.nextLine());

            switch(num){

                case 0:
                    value=false;
                    break;
                case 1:
                    DisplayProductsA(scan,gamesList,ad);
                    break;
                case 2:
                    UpdatePrice(scan);
                    break;
                case 3:
                    AddProduct(scan);
                    break;
                default:
                    System.out.println("invalid input");
            }
        }
    }

    //this method uses thew sql procedure to update the price of a game
    public static void UpdatePrice(Scanner scan){
        IReader listGame = new SqlImporter();
        List<Game> games = listGame.loadGames();
        for(int i = 0; i < games.size(); i++){
        System.out.println(i + ": " + games.get(i));
        }
        System.out.println("which game's price would you like to update? Please input the title of the game");
        String title = scan.nextLine();

        System.out.println("what new price would you like the game to be?");
        double newPrice = Double.parseDouble(scan.nextLine());

        listGame.updateGamePrice(title, newPrice);



    }


    public static void AddProduct(Scanner scan){
        IReader importer = new FileImporter();
        List<Game> game = importer.loadGames();
        System.out.println("What type of Game would you like to add, please enter Video Game or Board Game");
        String type = scan.nextLine();

            while (!type.equals("Video Game") && !type.equals("Board Game")) {
                type = scan.nextLine();
                System.out.println("Wrong input! Please enter Video Game or Board Game");
            }
                
                if(type.equals("Video Game")){

                    System.out.println("input the title");
                    String title = scan.nextLine();

                    System.out.println("input the price");
                    int price = Integer.parseInt(scan.nextLine());

                    System.out.println("input genre");
                    String genre = scan.nextLine();

                    System.out.println("input the minimum ammount of player");
                    int minPlayer = Integer.parseInt(scan.nextLine());

                    System.out.println("input the company");
                    String company = scan.nextLine();

                    System.out.println("input the memory size");
                    double memory = Double.parseDouble(scan.nextLine());

                    Game newGame = new VideoGame(type, title, price, genre, minPlayer, company, memory);
                    
                    importer.add(newGame);
                    System.out.println(game.get(game.size() - 1));
                }
                else if (type.equals("Board Game")){

                    System.out.println("input the title");
                    String title = scan.nextLine();
                    
                    System.out.println("input the price");
                    int price = Integer.parseInt(scan.nextLine());

                    System.out.println("input the genre");
                    String genre = scan.nextLine();

                    System.out.println("input the minimum ammount of player");
                    int minPlayer = Integer.parseInt(scan.nextLine());

                    System.out.println("input the company");
                    String company = scan.nextLine();

                    System.out.println("input the memory size");
                    double memory = Double.parseDouble(scan.nextLine());

                    Game newGame = new BoardGame(type, title, price, genre, minPlayer, company, memory);
                    importer.add(newGame);
                    System.out.println(game.get(game.size() - 1));
                }           
            }
        

    public static void DisplayProductsA(Scanner scan,List<Game> gamesList,IGameDisplayer ad)
    {
        boolean value=true;
        List<Game> videoGamesList = new ArrayList<>();
        List<Game> boardGamesList = new ArrayList<>();

        fillLists(videoGamesList, boardGamesList,gamesList);

        while(value)
        {
            System.out.println("0: Return");
            System.out.println("1: Display Board Games");
            System.out.println("2: Display Video Games");
            System.out.println("3: Display all Games");
            System.out.println("Enter Character");
            int num=scan.nextInt();

            switch(num){

                case 0:
                    value=false;
                    break;
                case 1:
                    DisplayBoardA(boardGamesList,ad);
                    break;
                case 2:
                    DisplayVideoA(videoGamesList,ad);
                    break;
                case 3:
                    DisplayProductsA(gamesList,ad);
                    break;
                default:
                    System.out.println("invalid input");
            }
        }
    }

    public static void DisplayBoardA(List<Game> boardGamesList,IGameDisplayer ad)
    {
        for (Game bg: boardGamesList)
        {
             ad.Display(bg);
        }
        
    }

    public static void DisplayVideoA(List<Game> videoGamesList,IGameDisplayer ad)
    {
        for (Game vg: videoGamesList)
        {
           ad.Display(vg);
        }
        
    }

    public static void DisplayProductsA(List<Game> gamesList,IGameDisplayer ad)
    {
        for (Game g: gamesList)
        {
             ad.Display(g);
        }
        
    }

    
    public static void Customer(Scanner scan)
    {
        IReader ir=new FileImporter();
        List<Game> gamesList=ir.loadGames();
        IGameDisplayer ud = new UserDisplay();
        List<Game> cart = new ArrayList<>();
        boolean value=true;
        while(value)
        {
            System.out.println("0: Return");
            System.out.println("1: Display Products");
            System.out.println("2: Add To Cart");
            System.out.println("3: Remove From Cart");
            System.out.println("4: display total From Cart");
            //remove
            System.out.println("Enter Character");
            int num = Integer.parseInt(scan.nextLine());

            switch(num){

                case 0:
                    value=false;
                    break;
                case 1:
                    DisplayProductsUser(scan,gamesList,ud);
                    break;
                case 2:
                    addToCart(scan,gamesList,cart);
                    break;
                case 3:
                    removeFromCart(scan,cart);
                    break;
                case 4:
                    checkTotal(cart,ud);
                    break;
                default:
                    System.out.println("invalid input");
            }
        }
    }

    public static void addToCart(Scanner scan,List<Game> gamesList,List<Game> cart)
    {
        
        boolean value=true;

        while(value){
            printTitleAndPrice(gamesList);
            System.out.println("enter product number you wish to add to cart.");
            int itemNum = Integer.parseInt(scan.nextLine()) - 1;
            String item = gamesList.get(itemNum).getTitle();

                boolean result =containscheck(gamesList,item);
                if(result)
                {
                    for (int i=0; i<gamesList.size();i++)
                    {
                        String game = gamesList.get(i).getTitle();
                        if (item.equals(game))
                        {
                            cart.add(gamesList.get(i));
                            System.out.println("Added to Cart");
                            System.out.println("the current total is $");
                            CalculateTotal(cart);
                        }
                    }
                }
                else{
                    System.out.println("not a recognizable game");
                    break;
                    }
                    boolean v2=true;
                while(v2){
                    System.out.println("0: return");
                    System.out.println("1: add to Cart again");
                    
                    int num=Integer.parseInt(scan.nextLine());
                    switch(num)
                    {
                        case 0:
                        v2=false;
                        value=false;
                            break;
                        case 1:
                        v2=false;
                            break;
                        default:
                            System.out.println("invalid input");
            

                    }
                }

        }
    }
//we didi this to give the person an easier way to insert. also this avoids typo issues

    public static void printTitleAndPrice(List<Game> gameList) {
        System.out.println("----------------------------------------------------------------");
        int i = 1;
        for (Game game : gameList) {
            System.out.println(i + " : Title: " + game.getTitle() + ", Price: $" + game.getPrice());
            i++;
        }
    }

    public static void checkTotal(List<Game> cart,IGameDisplayer ud)
    {
        DisplayProductsC(cart,ud);
        CalculateTotal(cart);
    }

    public static void removeFromCart(Scanner scan,List<Game> cart)
    {
        boolean value=true;
        printTitleAndPrice(cart);

        while(value)
        {
            System.out.println("enter product number that you would like ot remove.");
            int itemNum = Integer.parseInt(scan.nextLine()) - 1;
            String item = cart.get(itemNum).getTitle();
            
                boolean result =containscheck(cart,item);
                if(result)
                {
                    for (int i=0; i<cart.size();i++)
                    {
                        String game = cart.get(i).getTitle();
                        if (item.equals(game))
                        {
                            cart.remove(i);
                            System.out.println("removed from Cart");
                            System.out.println("the current total is $");
                            CalculateTotal(cart);
                        }
                    }
                }
                else{
                    System.out.println("not a recognizable game");
                    break;
                    }
                    boolean v2=false;
                while(v2)
                    System.out.println("0: return");
                    System.out.println("1: remove from Cart");
                    
                    int num=Integer.parseInt(scan.nextLine());
                    switch(num)
                    {
                        case 0:
                        v2=false;
                        value=false;
                            break;
                        case 1:
                        v2=false;
                            break;
                        default:
                            System.out.println("invalid input");
            

                    }
        }
    }
    //this method loops to check if the users inserted games exsists in the gamesList
    public static boolean containscheck(List<Game> gamesList,String item)
    {
         for (Game g: gamesList)
            {
                if(item==g.getTitle())
                {
                    return true;
                }
            }
            return false;
    }
    //calculates the price of the cart and prints the total
    public static void CalculateTotal(List<Game> cart)
    {
        double price=0;
        for (Game g:cart)
        {
            price+=g.getPrice();
        }
        System.out.println(price);
    }

    public static void DisplayProductsUser(Scanner scan,List<Game> gamesList, IGameDisplayer ud)
    {
        
        boolean value=true;
        //put into method
        List<Game> videoGamesList = new ArrayList<>();
        List<Game> boardGamesList = new ArrayList<>();
        fillLists(videoGamesList, boardGamesList,gamesList);

        while(value)
        {
            System.out.println("0: Return");
            System.out.println("1: Display Board Games");
            System.out.println("2: Display Video Games");
            System.out.println("3: Display all Games");
            System.out.println("Enter Character");
            int num=Integer.parseInt(scan.nextLine());

            switch(num){

                case 0:
                    value=false;
                    break;
                case 1:
                    DisplayBoardC(boardGamesList,ud);
                    break;
                case 2:
                    DisplayVideoC(videoGamesList,ud);
                    break;
                case 3:
                    DisplayProductsC(gamesList,ud);
                    break;
                default:
                    System.out.println("invalid input");
            }
        }
    }
    //this method seperats the gamesList that contains both board games and video games.we put each type in there specific lists so it would be easier and cler when we want to siplay in admin or customer
    public static void fillLists(List<Game> videoGamesList, List<Game> boardGamesList, List<Game> gamesList) {
        videoGamesList.clear();
        boardGamesList.clear();
    
        for (Game game : gamesList) {
            if (game instanceof VideoGame) {
                videoGamesList.add((VideoGame) game);
            } else if (game instanceof BoardGame) {
                boardGamesList.add((BoardGame) game);
            }
        }
    }
    public static void DisplayBoardC(List<Game> boardGamesList,IGameDisplayer ud)
    {
        for (Game bg: boardGamesList)
        {
             ud.Display(bg);
        }
        
    }

    public static void DisplayVideoC(List<Game> videoGamesList,IGameDisplayer ud)
    {
        for (Game vg: videoGamesList)
        {
           ud.Display(vg);
        }
        
    }

    public static void DisplayProductsC(List<Game> gamesList,IGameDisplayer ud)
    {
        for (Game g: gamesList)
        {
             ud.Display(g);
        }
        
    }
    

}
