package gamestore;
import java.util.List;


public interface IReader {
    public List<Game> loadGames();
    public void add(Game game);
    public void updateGamePrice(String title, double newPrice);
}