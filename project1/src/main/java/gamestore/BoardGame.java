package gamestore;

public class BoardGame extends Game{
    private double boardSize;

    public BoardGame(String type,String title, double price, String genre, int minPlayer, String company, double boardSize) {
        super(type,title,price,genre,minPlayer,company);
        this.boardSize = boardSize;
    }

    public double getBoardSize()
    {
        return this.boardSize;
    }

    @Override
    public String toString()
    {
        return super.toString()+", BoardSize: "+this.boardSize;
    }

}
