package gamestore;
import java.util.List;
import java.sql.*;


public class SqlImporter implements IReader{
    private List<Game> game;

    public void add(Game game){
        
    }

    public static Connection getConnection() throws SQLException{           
        String user=System.console().readLine("UserName: ");
        String password=new String(System.console().readPassword("Password: "));
        String url ="jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        Connection conn =DriverManager.getConnection(url,user,password);
        return conn;
    }

    public void updateGamePrice(String title, double newPrice) {
        try (Connection conn = getConnection()) {
            String updateProcedure = "{CALL updatePrice(?, ?)}";
            try (CallableStatement callableStatement = conn.prepareCall(updateProcedure)) {
                callableStatement.setDouble(1, newPrice);
                callableStatement.setString(2, title);
                callableStatement.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Game> loadGames() {

        try (Connection conn=getConnection()) {
            String sql = "SELECT * FROM Games";
            try (PreparedStatement statement = conn.prepareStatement(sql);
                 ResultSet resultSet = statement.executeQuery()) {

                while (resultSet.next()) {
                    String type = resultSet.getString("type");
                    String title = resultSet.getString("title");
                    double price = resultSet.getDouble("price");
                    String genre = resultSet.getString("genre");
                    int minPlayer = resultSet.getInt("minPlayer");
                    String company = resultSet.getString("company");
                    double boardSize = resultSet.getDouble("boardSize"); // assuming it's a typo in the SQL table name
                    double storageSize = resultSet.getDouble("storageSize");

                    // Assuming the 'type' column can be used to determine whether it's a BoardGame or VideoGame
                    if ("BoardGame".equals(type)) {
                        BoardGame boardGame = new BoardGame(type, title, price, genre, minPlayer, company, boardSize);
                        this.game.add(boardGame);
                    } else if ("VideoGame".equals(type)) {
                        VideoGame videoGame = new VideoGame(type, title, price, genre, minPlayer, company, storageSize);
                        this.game.add(videoGame);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle the exception appropriately in your application
        }

        return this.game;
    }
}
