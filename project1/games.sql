CREATE TABLE Games (
    title VARCHAR2(30) NOT NULL PRIMARY KEY,
    type VARCHAR2(20) NOT NULL,
    price NUMBER(100) NOT NULL,
    genre VARCHAR2(20) NOT NULL,
    minPlayer NUMBER(2) NOT NULL,
    company VARCHAR2(20) NOT NULL,
    boardSize NUMBER(3,2),
    storageSize NUMBER(3,2)

);

CREATE OR REPLACE PROCEDURE updatePrice(newPrice IN Games.price%TYPE, newTitle IN Games.title%TYPE)
AS
BEGIN
    UPDATE Games
    SET price = newPrice
    WHERE title = newTitle;
END;

CREATE OR REPLACE FUNCTION retrieveGames
    RETURN SYS_REFCURSOR
    IS
        cur SYS_REFCURSOR;
    BEGIN
    OPEN cur FOR
        SELECT * FROM Games;
    RETURN cur;
END;

-- Board Games
INSERT INTO Games (title, type, price, genre, minPlayer, company, boardSize, storageSize) 
VALUES ('The Game Of Life', 'Board Game', 30.62, 'Kids & Family Games', 2, 'Hasbro', 81, NULL);

INSERT INTO Games (title, type, price, genre, minPlayer, company, boardSize, storageSize) 
VALUES ('Candy Land', 'Board Game', 11.60, 'Kids & Family Games', 2, 'Hasbro', 133.17, NULL);

INSERT INTO Games (title, type, price, genre, minPlayer, company, boardSize, storageSize) 
VALUES ('Hungry Hungry Hippos', 'Board Game', 21.92, 'Classic Games', 2, 'Hasbro', 509.22, NULL);

INSERT INTO Games (title, type, price, genre, minPlayer, company, boardSize, storageSize) 
VALUES ('KerPlunk', 'Board Game', 14.97, 'Classic Games', 2, 'mattel GAMES', 94.5, NULL);

INSERT INTO Games (title, type, price, genre, minPlayer, company, boardSize, storageSize) 
VALUES ('Lets Go Fishin', 'Board Game', 16.99, 'Kids & Family Games', 2, 'Pressman Toys', 220, NULL);

-- Video Games
INSERT INTO Games (title, type, price, genre, minPlayer, company, boardSize, storageSize) 
VALUES ('Spider-Man 2', 'Video Game', 69.99, 'Action & Adventure', 1, 'Sony', NULL, 98);

INSERT INTO Games (title, type, price, genre, minPlayer, company, boardSize, storageSize) 
VALUES ('Mortal Kombat 1', 'Video Game', 69.99, 'Fighting', 1, 'Warner Bros.', NULL, 114);

INSERT INTO Games (title, type, price, genre, minPlayer, company, boardSize, storageSize) 
VALUES ('A Way Out', 'Video Game', 37.64, 'Narrative', 2, 'Electronic Arts', NULL, 25);

INSERT INTO Games (title, type, price, genre, minPlayer, company, boardSize, storageSize) 
VALUES ('Hogwarts Legacy', 'Video Game', 69.99, 'Action & Adventure', 1, 'Warner Bros.', NULL, 85);

INSERT INTO Games (title, type, price, genre, minPlayer, company, boardSize, storageSize) 
VALUES ('Mario Kart 8', 'Video Game', 69.98, 'Racing', 1, 'Nintendo', NULL, 6.8);

